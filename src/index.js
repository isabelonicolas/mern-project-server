/**
|--------------------------------------------------
| IMPORTS
|--------------------------------------------------
*/
const dotenv = require("dotenv");
const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");

/**
|--------------------------------------------------
| CONFIGS
|--------------------------------------------------
*/
dotenv.config();
const app = express();
require("./database");

/**
|--------------------------------------------------
| MIDDLEWARE
|--------------------------------------------------
*/
app.use(express.static("public"));
app.use("/uploads", express.static("uploads"));
app.use(cors());

// parsing application/json
app.use(bodyParser.json());

// parsing application/x-www-form-urlencoded
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

/**
|--------------------------------------------------
| URL REQUEST LOGGER
|--------------------------------------------------
*/
app.use((req, res, next) => {
  console.log(`${new Date().toString()} => ${req.originalUrl}`);
  next();
});

/**
|--------------------------------------------------
| API
|--------------------------------------------------
*/
const routes = [
  { url: "/api/user", requirePath: "./api/user" },
  { url: "/api/playground", requirePath: "./api/playground" }
];

routes.forEach(route => {
  app.use(route.url, require(route.requirePath));
});

/**
|--------------------------------------------------
| ERROR HANDLERS
|--------------------------------------------------
*/
// Error 404
app.use((req, res, next) => {
  res.json({
    error: {
      code: 404,
      message: "Not Found"
    }
  });
});

// Error 500
app.use((err, req, res, next) => {
  res.status(500).json({
    error: {
      code: 500,
      message: "Internal Server Error"
    }
  });
});

/**
|--------------------------------------------------
| RUN SERVER
|--------------------------------------------------
*/
app.listen(process.env.PORT, () => console.info(`Server is currently running on port ${process.env.PORT}`));
