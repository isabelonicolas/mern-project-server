const mongoose = require("mongoose");
const util = require("../helpers/util");

/*===== Model =====*/
const UserSchema = new mongoose.Schema(
  {
    firstName: {
      type: String,
      trim: true
    },
    lastName: {
      type: String,
      trim: true
    },
    gender: {
      type: String,
      trim: true
    }
  },
  { timestamps: true }
);

/*===== Middleware =====*/
UserSchema.pre("save", function(next) {
  this.firstName = util.capitalize(this.firstName);
  this.lastName = util.capitalize(this.lastName);
  next();
});

/*===== Methods =====*/
UserSchema.statics.fetchUsers = async function fetchUsers() {
  return new Promise((resolve, reject) => {
    this.find().exec((err, doc) => {
      if (err) {
        return reject({ error: err });
      }
      resolve(doc);
    });
  });
};

UserSchema.statics.fetchUserById = async function fetchUserById(id) {
  return new Promise((resolve, reject) => {
    this.findById(id).exec((err, doc) => {
      if (err) {
        return reject({ error: err });
      }
      resolve(doc);
    });
  });
};

// UserModel.findById(req.params.id, function(err, data) {
//   res.json(data);
// });

// UserSchema.statics.getUsers = async function getUsers() {
//   return new Promise((resolve, reject) => {
//     this.aggregate([
//       {
//         $lookup: {
//           from: "imageuploads",
//           localField: "_id",
//           foreignField: "owner",
//           as: "avatar"
//         }
//       },
//       { $unwind: "$avatar" }
//     ]).exec((err, doc) => {
//       if (err) {
//         return reject({ error: err });
//       }

//       resolve(doc);
//     });
//   });
// };

module.exports = mongoose.model("User", UserSchema);
