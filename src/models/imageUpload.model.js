const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ImageUploadSchema = new Schema({
  owner: {
    type: Schema.Types.ObjectId,
    trim: true
  },
  kind: {
    type: String,
    trim: true
  },
  fileName: {
    type: String,
    trim: true
  },
  isDeleted: {
    type: Boolean,
    default: false
  },
  dateDeleted: Number,
  dateCreated: {
    type: Number,
    default: Math.floor(Date.now() / 1000)
  },
  dateUpdated: {
    type: Number,
    default: Math.floor(Date.now() / 1000)
  }
})

module.exports = mongoose.model('ImageUpload', ImageUploadSchema)