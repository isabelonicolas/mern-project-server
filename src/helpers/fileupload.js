const multer = require("multer")

let storage = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, "uploads")
  },
  filename: (req, file, callback) => {
    callback(null, file.fieldname + '-' + Date.now() + '.' + file.mimetype.split('/')[1])
  }
})

let upload = multer({ storage })

module.exports = upload