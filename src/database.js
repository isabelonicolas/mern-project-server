const mongoose = require("mongoose");
const conn = "mongodb+srv://root:root@cluster0-czqgx.mongodb.net/db_mern";

mongoose.Promise = global.Promise;
mongoose.connect(conn, { useNewUrlParser: true, useUnifiedTopology: true }, err => {
  if (err) {
    console.log("Database connection failed.");
  } else {
    console.log("Database connection established.");
  }
});
