/**
 * PLAYGROUND API
 * @author Isabelo Nicolas <isabelo.nicolas@gmail.com>
 */
const express = require("express");
const axios = require("axios");
const PlaygroundAPI = express.Router();

PlaygroundAPI.get("/", async (req, res) => {
  try {
    let YesNo = await axios.get("https://yesno.wtf/api");
    let data = YesNo.data;

    res.status(200).json({
      data: data
    });
  } catch (e) {
    return res.json({
      type: "error",
      message: e.message || e.error
    });
  }
});

module.exports = PlaygroundAPI;
