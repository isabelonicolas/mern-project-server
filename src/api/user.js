/**
 * USER API
 * @author Isabelo Nicolas <isabelo.nicolas@gmail.com>
 */
const express = require("express");
// const mongoose = require("mongoose");

const UserAPI = express.Router(),
  UserModel = require("../models/user.model"),
  ImageUploadModel = require("../models/imageUpload.model"),
  ImageUpload = require("../helpers/fileupload");

/**
|--------------------------------------------------
| FETCH
|--------------------------------------------------
*/
UserAPI.get("/", async (req, res) => {
  try {
    let users = await UserModel.fetchUsers();

    res.status(200).json({
      data: users
    });
  } catch (e) {
    return res.json({
      error: {
        message: e.message || e.error
      }
    });
  }
});

UserAPI.get("/:id", async (req, res) => {
  try {
    let user = await UserModel.fetchUserById(req.params.id);

    res.status(200).json({
      data: user
    });
  } catch (e) {
    return res.json({
      error: {
        message: e.message || e.error
      }
    });
  }
});

/**
|--------------------------------------------------
| POST
|--------------------------------------------------
*/
UserAPI.post("/", ImageUpload.single("avatar"), async (req, res) => {
  try {
    // if (!req.file) {
    //   throw new Error("Please upload a file");
    // }

    const user = new UserModel(req.body);

    await user.save(err => {
      if (err) throw new Error("Unable to save data to the database.");
    });

    // const image = new ImageUploadModel({
    //   owner: mongoose.Types.ObjectId(user.id),
    //   kind: "user-avatar",
    //   fileName: req.file.filename
    // });
    // await image.save();

    return res.json({
      data: {
        message: "User has been created."
      }
    });
  } catch (e) {
    return res.json({
      error: {
        message: e.message || e.error
      }
    });
  }
});

/**
|--------------------------------------------------
| PUT
|--------------------------------------------------
*/
UserAPI.put("/:id", async (req, res) => {
  try {
    let user = await UserModel.fetchUserById(req.params.id);

    user.firstName = req.body.firstName;
    user.lastName = req.body.lastName;
    user.gender = req.body.gender;

    await user.save(err => {
      if (!err) {
        res.status(200).json({
          data: {
            message: "User has been updated."
          }
        });
      } else {
        throw new Error("Unable to save data to the database.");
      }
    });
  } catch (e) {
    return res.json({
      error: {
        message: e.message || e.error
      }
    });
  }
});

/**
|--------------------------------------------------
| DELETE
|--------------------------------------------------
*/
UserAPI.delete("/:id", async (req, res) => {
  try {
    await UserModel.findOneAndRemove({_id: req.params.id});
    return res.json({
      data: {
        message: "User has been deleted."
      }
    });
  } catch {
    return res.json({
      error: {
        message: e.message || e.error
      }
    });
  }
});

module.exports = UserAPI;
